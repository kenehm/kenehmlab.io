Title: SETTING-UP Pelican on Locally
Date: 2016-03-25
Category: GitLab
Tags: pelican, gitlab
Slug: pelican-on-gitlab-pages

This site is hosted on GitLab Pages!

The source code of this site is at <https://gitlab.com/kenehm/kenehm.gitlab.io>.

Learn about GitLab Pages at <https://pages.gitlab.io>.
